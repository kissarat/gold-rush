<?php

namespace app\controllers;

use app\behaviors\Access;
use Yii;
use app\models\Translation;
use app\models\search\Translation as TranslationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class TranslationController extends Controller
{
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::className(),
                'manager' => ['index', 'view', 'create', 'update', 'delete'],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new TranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate() {
        $model = new Translation();

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->db->createCommand('INSERT INTO source_message(message) VALUES (:message)', [
                ':message' => $model->message
            ])->execute();
            $id = Yii::$app->db->getLastInsertID();
            Yii::$app->db->createCommand('INSERT INTO message(id, translation) VALUES (:id, :translation)', [
                ':id' => $id,
                ':translation' => $model->translation
            ])->execute();
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)  {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->db->createCommand('UPDATE source_message SET message = :message WHERE id = :id', [
                ':id' => $model->id,
                ':message' => $model->message
            ])->execute();
            Yii::$app->db->createCommand('UPDATE message SET translation = :translation WHERE id = :id', [
                ':id' => $model->id,
                ':translation' => $model->translation
            ])->execute();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        Yii::$app->db->createCommand('DELETE FROM source_message WHERE id = :id', [
            ':id' => $id
        ])->execute();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Translation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Translation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Translation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
