<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/23/15
 * Time: 2:23 AM
 */

namespace app\controllers;


use app\models\Article;
use app\models\Record;
use app\models\search\Invoice;
use app\models\User;
use app\models\UserData;
use app\SQL;
use Yii;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\ForbiddenHttpException;

class HomeController extends Controller {
    public function actionIndex() {
        return $this->render('index', [
            'marketing' => $this->renderPartial('page', ['model' => Article::fromFile('marketing')]),
            'statistics' => $this->renderPartial('statistics', ['statistics' => static::statistics()]),
        ]);
    }

    public function actionLanguage($code = 'en') {
        $cookies = Yii::$app->response->cookies;
        if ('en' == $code) {
            $cookies->add(new Cookie([
                'name' => 'lang',
                'value' => 'en',
                'expire' => time() + 3600 * 24 * 30
            ]));
        }
        else {
            $cookies->remove('lang');
        }
        return $this->goBack();
    }

    public function actionPage($name) {
        $article = Article::fromFile($name);
        if (false !== $article->content) {
            return $this->render('page', [
                'model' => $article
            ]);
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Page not found.'));
            return $this->render('index');
        }
    }

    public function actionUpdate($name) {
        $article = new Article();
        if ($article->load(Yii::$app->request->post()) && $article->validate() && $article->saveFile()) {
            $this->redirect(['page', 'name' => $article->name]);
        }
        else {
            $article->name = $name;
            $article->loadFile();
        }

        if (false !== $article->content) {
            return $this->render('update', [
                'model' => $article
            ]);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Page not found.'));
            return $this->render('index');
        }
    }

    public function actionError() {
        $exception = Yii::$app->getErrorHandler()->exception;
        $message = $exception->getMessage();
        if ($exception instanceof ForbiddenHttpException) {
            $message = $message ? Yii::t('app', $message) : Yii::t('app', 'Forbidden');
        }
        if ($message) {
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->render('error', [
            'exception' => $exception
        ]);
    }

    public static function statistics() {
        $started = strtotime(Record::find()->min('time'));
        $invested = SQL::query('SELECT count(*) FROM
            (SELECT id, if(reinvest_from is null, 0, 1) as reinvest  FROM ((SELECT node_id as id, reinvest_from FROM archive)
            UNION (SELECT id, reinvest_from FROM node)) a GROUP BY id) g GROUP BY reinvest ORDER BY reinvest');
        $invested = $invested->queryColumn();
        return [
            'Started' => date('d-m-Y', $started),
            'Running days' => floor((time() - $started)/(3600 * 24)),
            'Users' => User::find()->count(),
            'Total deposited' => Invoice::find()->where(['or',
                ['status' => 'success'],
                ['status' => 'delete']
            ])->andWhere('amount > 0')->sum('amount'),
            'Total withdraw' => - Invoice::find()->where(['or',
                ['status' => 'success'],
                ['status' => 'delete']
            ])->andWhere('amount < 0')->sum('amount'),
            'Number of investments' => isset($invested[0]) ? $invested[0] : 0,
            'Number of reinvestments' => isset($invested[1]) ? $invested[1] : 1
        ];
    }
}
