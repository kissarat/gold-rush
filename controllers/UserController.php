<?php

namespace app\controllers;

use app\behaviors\Access;
use app\behaviors\Journal;
use app\models\Login;
use app\models\Password;
use app\models\Record;
use app\models\ResetRequest;
use Yii;
use app\models\User;
use app\models\search\User as UserSearch;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UserController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::className(),
                'plain' => ['view', 'update'],
                'manager' => ['index', 'complete'],
                'admin' => ['delete']
            ]
        ];
    }

    public function actions() {
        return [
            'social' => [
                'successCallback' => [$this, 'social']
            ]
        ];
    }

    public function actionIndex() {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($name = null) {
        if (!$name && Yii::$app->user->isGuest) {
            throw new InvalidParamException();
        }

        $model = $name ? $this->findModel($name) : Yii::$app->user->identity;
        if (Yii::$app->user->isGuest
            || (!Yii::$app->user->identity->isManager()
            && Yii::$app->user->identity->name != $model->name)) {
            throw new ForbiddenHttpException();
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate() {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'name' => $model->name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($name) {
        $model = $this->findModel($name);

        if ($model->name != Yii::$app->user->identity->name && !Yii::$app->user->identity->isAdmin()) {
            throw new ForbiddenHttpException('Forbidden');
        }

        if ($model->load(Yii::$app->request->post())) {
            if (!Yii::$app->user->identity->isAdmin() && $model->isAttributeChanged('perfect', false)) {
                Yii::$app->session->setFlash('error', 'Wallet can change admin only');
            }
            elseif ($model->save()) {
                return $this->redirect(['view', 'name' => $model->name]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($name) {
        $this->findModel($name)->delete();
        return $this->redirect(['index']);
    }

    /**
     * @param string $name
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($name) {
        if (($model = User::findOne(['name' => $name])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSignup($referral = null) {
        $model = new User([
            'scenario' => 'signup',
            'referral' => $referral
        ]);
        if (preg_match('|U\w{7}|', $model->name)) {
            $model->addError('name', Yii::t('app', 'Username cannot be in the format of Perfect Money wallet'));
        }
        elseif ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->generateCode();
            $model->status = User::PLAIN;
            $model->save(false);

            if (Yii::$app->user->login($model)) {
                return $this->redirect(['user/view']);
            }
            else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Something wrong happened'));
                return $this->redirect(['home/index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionLogin() {
        $model = new Login();

        if ($model->load(Yii::$app->request->post())) {
            $user = $model->getUser();
            if ($user) {
                $can = $user->canLogin();
                if ($can && $user->validatePassword($model->password)) {
                    if ($user->status > 0) {
                        if (empty($user->auth)) {
                            $user->generateAuthKey();
                            $user->save();
                        }
                        Yii::$app->user->login($user);
//                        return $this->redirect(['view', 'name' => $user->name]);
                        return $this->redirect(['home/index']);
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', Yii::t('app', 'Your account is blocked')));
                    }
                }
                else {
                    Journal::write('user', 'login_fail', $user->id);
                    if ($can) {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
                    }
                    else {
                        $record = Record::find()->where([
                            'object_id' => $user->id,
                            'event' => 'login_fail'
                        ])->orderBy(['time' => SORT_DESC])->one();
                        Yii::$app->session->setFlash('error',
                            Yii::t('app', 'You have exceeded the maximum number of login attempts, you will be able to enter after {time}', [
                                'time' => $record->time
                            ]));
                    }
                }
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid username or password'));
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->redirect(['home/index']);
    }

    public function actionPassword($code = null, $name = null) {
        $message = null;
        $model = null;
        if (isset($_POST['name'])) {
            $name = $_POST['name'];
        }
        if ($name) {
            if (Yii::$app->user->identity->isAdmin() || $name == Yii::$app->user->identity->name) {
                $user = User::findOne(['name' => $name]);
            }
            else {
                throw new ForbiddenHttpException();
            }
        }
        else {
            $user = $code ? User::findOne(['code' => $code]) : Yii::$app->user->identity;
        }
        if ($user) {
            $model = new Password([
                'scenario' => ($code || $name) ? 'reset' : 'default',
                'user' => $user
            ]);

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ('reset' == $model->scenario) {
                    $user->code = null;
                    if (!$user->auth) {
                        $user->generateAuthKey();
                    }
                    $user->setPassword($model->new_password);
                    if ($user->save()) {
                        Yii::$app->session->addFlash('success', Yii::t('app', 'Password saved'));
                        return Yii::$app->user->isGuest
                            ? $this->redirect(['user/login'])
                            : $this->redirect(['user/view', 'name' => $user->name]);
                    } else {
                        $message = json_encode($user->errors, JSON_PRETTY_PRINT, JSON_UNESCAPED_UNICODE);
                    }
                } else {
                    if ($user->validatePassword($model->password)) {
                        $user->setPassword($model->new_password);
                        if ($user->save()) {
                            Yii::$app->session->addFlash('success', Yii::t('app', 'Password saved'));
                            return $this->redirect(['user/view', 'name' => $user->name]);
                        }  else {
                            $message = json_encode($user->errors, JSON_PRETTY_PRINT, JSON_UNESCAPED_UNICODE);
                        }
                    }
                    else {
                        $model->addError('password', Yii::t('app', 'Invalid password'));
                    }
                }
            }
        }
        else {
            $message = Yii::t('app', 'Invalid code');
        }
        return $this->render('password', [
            'model' => $model,
            'message' => $message
        ]);
    }

    public function actionRequest() {
        $model = new ResetRequest();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::findOne(['email' => $model->email]);
            $user->generateCode();
            if ($user->save()) {
                $url = Url::to(['password', 'code' => $user->code], true);
                if($user->sendEmail('html', [
                    'subject' => Yii::$app->params['site']['name'] . ' ' . Yii::t('app', 'Password reset'),
                    'content' => Yii::t('app', 'To recover your password, open <a href="{url}">this link</a>', [
                        'url' => $url,
                    ])
                ])) {
                    Yii::$app->session->setFlash('info', Yii::t('app', 'Check your email'));
                    return $this->redirect(['home/index']);
                }
                else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Failed to send mail'));
                }
            }
            else {
                Yii::$app->session->setFlash('error', json_encode($user->errors));
            }
        }
        return $this->render('request', [
            'model' => $model
        ]);
    }



    public function actionComplete($search) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return User::find()
            ->select('name')
            ->where('name like :name', [
                ':name' => "$search%"
            ])
            ->limit(10)
            ->column();
    }
}
