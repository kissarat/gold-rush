<?php

namespace app\controllers;

use app\behaviors\Access;
use app\models\Invoice;
use app\models\Matrix;
use app\models\Node;
use app\models\search\Income as IncomeSearch;
use app\models\User;
use Exception;
use Yii;
use app\models\Type;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TypeController implements the CRUD actions for Type model.
 */
class MatrixController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::className(),
                'manager' => ['create', 'update', 'delete', 'up', 'rise']
            ],

            'cache' => [
                'class' => 'yii\filters\PageCache',
                'only' => ['index', 'income'],
                'duration' => 300,
                'enabled' => true,
                'variations' => [
                    array_merge(['page' => '1', 'size' => '15', 'order' => 'asc'], $_GET),
                    Yii::$app->user->isGuest
                ],
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT max(`time`) + count(*) FROM node',
                ],
            ],
        ];
    }

    public function actionIndex($page = 1, $size = 15, $order = 'asc') {
        $count = Matrix::find()->select('count(*) as count')->groupBy('type_id')->column();
        $count = empty($count) ? 0 : (int) max($count);
        if ((Yii::$app->user->isGuest || !Yii::$app->user->identity->isManager()) && $size > 100) {
            return $this->redirect(['index', 'size' => 15]);
        }
        $pages = [
            'totalCount' => $count,
            'pageSize' => $size,
            'pageSizeParam' => 'size',
            'page' => $page - 1
        ];

        $pages = new Pagination($pages);

        /** @var Query $query */
        $query = null;
        $table = [];
        $types = Type::all();
        $order = 'asc' == $order ? SORT_ASC : SORT_DESC;
        foreach($types as $type) {
            $subquery = Matrix::find()
                ->where(['type_id' => $type->id])
                ->offset($pages->offset)
                ->limit($pages->limit)
                ->orderBy(['time' => $order, 'id' => $order]);
            if ($query) {
                $query->union($subquery);
            }
            else {
                $query = $subquery;
            }
            $table[$type->id - 1] = [];
        }
        foreach($query->all() as $invest) {
            $table[$invest->type_id - 1][] = $invest->attributes;
        }

        return $this->render('index', [
            'models' => $table,
            'pages' => $pages
        ]);
    }

    public function actionPlan() {
        $dataProvider = new ActiveDataProvider([
            'query' => Type::find(),
        ]);

        return $this->render('plan', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInvest($user = null, $id = null) {
        if (!$user && !Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager()) {
            return $this->redirect(['invest', 'user' => Yii::$app->user->identity->name]);
        }
        $parent = $id ? $this->findNode($id) : null;
        $query = Node::find()
            ->with('type')
            ->orderBy(['time' => SORT_ASC, 'id' => SORT_ASC]);
        if ($user) {
            $query->andWhere(['user_name' => $user]);
        }
        elseif ($parent) {
            $query
                ->andWhere('time < :time', [':time' => $parent->time])
                ->andWhere(['type_id' => $parent->type_id]);
        }
        return $this->render('invest', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query
            ]),
            'parent' => $parent,
            'user' => $user
        ]);
    }

    public function actionRise() {
        Node::rise();
    }

    public function actionUp($id) {
        $invest = $this->findNode($id);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $invest->up();
            $transaction->commit();
        }
        catch (Exception $ex) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->redirect(['invest', 'id' => $id]);
    }

    public function actionIncome() {
        $searchModel = new IncomeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('income', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Node();

        if ($model->load(Yii::$app->request->post())) {
            $model->time = $_SERVER['REQUEST_TIME'];
            if (empty($model->count)) {
                $model->count = $model->type->degree;
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        $errors = $model->getErrors();
        if ($errors) {
            Yii::$app->session->setFlash('error', json_encode($model->getErrors()), JSON_UNESCAPED_UNICODE);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findNode($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['invest', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findNode($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionOpen($id) {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $node = new Node([
            'user' => $user,
            'type_id' => $id
        ]);

        if ($user->account >= $node->type->stake) {
            $user->account -= $node->type->stake;
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($id < 3 && $user->referral && Node::find()->where(['user_name' => $user->name])->count() <= 0) {
                    $user->referralUser->account += $node->type->stake * 0.05;
                    $user->referralUser->update(true, ['account']);
                }
                if ($user->update(true, ['account']) && $node->invest()) {
                    do {
                        $i = $node->rise();
                    } while ($i > 0);
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t('app', 'The plan is open'));
                }
            }
            catch(Exception $ex) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $ex->getMessage());
            }
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', Invoice::$statuses['insufficient_funds']));
            return $this->redirect(['plan']);
        }

        return $this->redirect(['invest', 'user' => Yii::$app->user->identity->name]);
    }

    public function actionNode($id) {

        return $this->render('node', [
            'model' => Node::findOne($id)
        ]);
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Type the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Type::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Node the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findNode($id) {
        if (($model = Node::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
