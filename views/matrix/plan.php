<?php

use app\models\Type;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Plans');

$columns = [
    [
        'attribute' => 'name',
        'format' => 'html',
        'value' => function(Type $model) {
            return Html::a($model, ['view', 'id' => $model->id]);
        }
    ],
    'stake',
    'degree',
    'income',
    [
        'attribute' => 'invest_id',
        'format' => 'html',
        'value' => function(Type $model) {
            return $model->getInvest();
        }
    ],
    'reinvest'
];

if (!Yii::$app->user->isGuest) {
    $columns[] = [
        'label' => Yii::t('app', 'Action'),
        'format' => 'html',
        'value' => function($model) {
            if(Yii::$app->user->identity->account >= $model->stake) {
                return Html::a('Open', ['view', 'id' => $model->id], ['class' => 'btn btn-success btn-sm']);
            }
            else {
                return 'Недостаточно средств';
            }
        }
    ];
}
?>
<div class="type-index">

    <h1 class="bagatelle"><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
            echo Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns
    ]); ?>

</div>
