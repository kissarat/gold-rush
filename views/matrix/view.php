<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Type */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plans'), 'url' => ['plan']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()): ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php
        endif;

        $form = ActiveForm::begin(['action' => ['open', 'id' => $model->id]]);
        echo Html::button(Yii::t('app', 'Open'), ['type' => 'submit', 'class' => 'btn btn-success']);
        ActiveForm::end();
        ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => Html::a($model, ['view', 'id' => $model->id])
            ],
            'stake',
            'income',
            [
                'attribute' => 'next_id',
                'format' => 'html',
                'value' => $model->next_id ? Html::a($model->next, ['view', 'id' => $model->next_id]) : null
            ]
        ],
    ]) ?>

</div>
