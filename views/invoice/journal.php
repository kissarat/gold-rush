<?php

/**
 * @var app\models\Record $record
 */

use app\models\Invoice;

$info = $record->info;
if ($info) {
    if (isset($info['status'])) {
        echo Yii::t('app', Invoice::$statuses[$info['status']]);
    }
}
