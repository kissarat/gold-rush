<?php
use app\Alert;
use app\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$login = Yii::$app->user->isGuest ? '' : 'login';
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
//throw new Exception(!!Yii::$app->user->identity->isManager())
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="image" content="/image/background.jpg" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image" href="/image/background.jpg" type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<div class="background"></div>
<div class="navbar-border"></div>
<?php $this->beginBody() ?>
<div class="wrap <?= $login ?>">
    <?php
    NavBar::begin([
        'brandLabel' =>
            ($manager ? '' : 'GOLD RUSH RUSSIA'),
        'brandUrl' => Yii::$app->homeUrl,
    ]);

    $invest_url = ['/matrix/invest'];
    if (!Yii::$app->user->getIsGuest() && !Yii::$app->user->identity->isManager()) {
        $invest_url['user'] = Yii::$app->user->identity->name;
    }

    $items = [
//        ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index'], 'options' => ['class' => 'hideable']],
        ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/create']],
        ['label' => Yii::t('app', 'FAQ'), 'url' => ['/faq/index']],
        ['label' => Yii::t('app', 'Queue'), 'url' => ['/matrix/index']],
        ['label' => Yii::t('app', 'Marketing'), 'url' => ['/matrix/plan']]
    ];

    if (Yii::$app->user->isGuest) {
        $items[] = ['label' => Yii::t('app', 'Signup') , 'url' => ['/user/signup']];
        $items[] = ['label' => Yii::t('app', 'Login') , 'url' => ['/user/login']];
    }
    else {
        $items[] = ['label' => Yii::t('app', 'Income'), 'url' => ['/matrix/income']];
        $items[] = ['label' => Yii::t('app', 'Investment'), 'url' => $invest_url];
        $items[] = ['label' => Yii::t('app', 'Payment') , 'url' =>['/invoice/index']];
        $items[] = ['label' => Yii::t('app', 'Profile') , 'url' => ['/user/view']];
        if ($manager) {
            $items[] = ['label' => Yii::t('app', 'Translation') , 'url' => ['/translation/index']];
        }
        $items[] = [
            'label' => Yii::t('app', 'Logout'),
            'url' => ['/user/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }

    $items[] = empty($_COOKIE['lang'])
        ? ['label' => 'EN', 'url' => ['/home/language', 'code' => 'en'], 'options' => ['title' => 'English']]
        : ['label' => 'RU', 'url' => ['/home/language', 'code' => 'ru'], 'options' => ['title' => 'Русский']];

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>


    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    Разработано <a href="http://zenothing.com/">zenothing.com</a>
</footer>

<script src="/script.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
