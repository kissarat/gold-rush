<?php

use app\Ext;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::$app->user->isGuest ? Yii::t('app', 'Signup') : Yii::t('app', 'Create User');
$this->registerMetaTag([
    'name' => 'robots',
    'content' => 'noindex'
]);
?>
<div class="user-create">
    <?= Ext::stamp() ?>

    <h1 class="bagatelle"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
