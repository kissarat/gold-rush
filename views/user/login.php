<?php

use app\Ext;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Login */
/* @var $form ActiveForm */
$this->title = Yii::t('app', 'Login');
$this->registerMetaTag([
    'name' => 'robots',
    'content' => 'noindex'
]);
?>
<div class="user-login">
    <?= Ext::stamp() ?>

    <h1 class="bagatelle"><?= $this->title ?></h1>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>

    <div class="form-group">
        <?= Yii::t('app', 'You can recover <a href="{url}">your password</a>', [
            'url' => Url::to(['request']),
        ]); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
