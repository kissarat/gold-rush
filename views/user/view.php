<?php

use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->name;
if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}

$columns = [
    'name',
    'email:email',
    'account',
    'phone',
    'skype',
    'perfect'
];

if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
    $columns[] = [
        'attribute' => 'status',
        'value' => User::statuses()[$model->status]
    ];
}

if ($model->referral) {
    $columns[] = 'referral';
}

$referral = Url::to(['/user/signup', 'referral' => $model->name], true);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?= Html::a(Yii::t('app', 'Journal'),
            ['journal/index', 'user' => $model->name], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Investment'),
            ['matrix/invest', 'user' => $model->name], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Income'),
            ['matrix/income', 'user' => $model->name], ['class' => 'btn btn-primary']) ?>
        <?php
        if (!Yii::$app->user->isGuest) {
            if ($model->name == Yii::$app->user->identity->name || Yii::$app->user->identity->isAdmin()) {
                echo Html::a(Yii::t('app', 'Change Password'),
                    ['user/password', 'name' => $model->name], ['class' => 'btn btn-warning']);
            }
            ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'name' => $model->name], ['class' => 'btn btn-primary']) ?>
            <?php
            if (Yii::$app->user->identity->isAdmin()) {
                echo Html::a(Yii::t('app', 'Delete'), ['delete', 'name' => $model->name
                ], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]);
            }
        }
        ?>
    </div>

    <div>
        <?= Html::a(Yii::t('app', 'Referral Link'), $referral, ['class' => 'form-label']); ?>
        <input class="form-control" value="<?= Url::to($referral, true); ?>">
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $columns,
    ]) ?>

</div>
