<?php

use app\AjaxComplete;
use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

//throw new Exception(json_encode($model->errors));

function submit($label) {
    echo '<div class="form-group">';
    echo Html::submitButton($label, ['class' => 'btn btn-success']);
    echo '</div>';
}

$form = ActiveForm::begin();

if ($model->referral) {
    echo Yii::t('app', 'Referral') . ': ' , $model->referral;
    echo Html::tag('div', Html::activeHiddenInput($model, 'referral'), ['class' => 'form-group']);
}

echo $form->field($model, 'name');
echo $form->field($model, 'email');
if ('signup' == $model->scenario) {
    echo $form->field($model, 'password')->passwordInput();
}
echo $form->field($model, 'phone');
echo $form->field($model, 'skype');
if ($model->isNewRecord || Yii::$app->user->identity->isAdmin()) {
    echo $form->field($model, 'perfect');
}
else {
    $a = Html::a('обратитесь к адмнинистратору', ['feedback/create', 'template' => 'wallet']);
    echo "<div class='form-group'>Для изменения кошелька $a</div>";
}
if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
    echo $form->field($model, 'account');
    echo $form->field($model, 'status')->dropDownList(User::statuses());
    echo $form->field($model, 'referral')->widget(AjaxComplete::class, [
        'route' => '/user/complete',
        'options' => [
            'class' => 'form-control'
        ]
    ]);
}

if (Yii::$app->user->isGuest) {
    submit(Yii::t('app', 'Signup'));
}
else {
    if ($model->isNewRecord) {
        submit(Yii::t('app', 'Create'));
    }
    else {
        submit(Yii::t('app', 'Update'));
    }
}

ActiveForm::end();

echo '</div>';
