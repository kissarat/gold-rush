<?php

use app\Ext;
use yii\helpers\Html;

/* @var $exception \app\JournalException */

$this->title = $exception->event;
$this->registerMetaTag([
    'name' => 'robots',
    'content' => 'noindex'
]);
?>
<div class="site-error">
    <?= Ext::stamp() ?>

    <h1><?= Html::encode($this->title) ?></h1>
</div>
