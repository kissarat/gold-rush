<?php
use yii\helpers\Html;
?>
<div class="statistics">
    <table>
<?php
foreach($statistics as $name => $value) {
    echo Html::tag('tr', Html::tag('td', Yii::t('app', $name) . ':') . Html::tag('td', $value));
}
?>
    </table>
</div>
