<?php
use app\Ext;
use yii\helpers\Html;

/**
 * @var string $statistics
 * @var string $marketing
 */
?>
<div class="home-index">
    <?= Ext::stamp() ?>

    <div class="index-row">
        <div>
            <img src="/img/b.png" />
            <?= $statistics ?>
            <img src="/img/b.png" />
            <?= Html::a(Yii::t('app', 'Signup'), ['user/signup'], ['class' => 'signup']) ?>
        </div>
        <div class="border">
            <div class="gallery"></div>
        </div>
    </div>
    <?= $marketing ?>
</div>