var start = Date.now();

function $$(selector) { return document.querySelector(selector) }
function $all(selector) { return document.querySelectorAll(selector) }
function each(selector, call) {
    return Array.prototype.forEach.call($all(selector), call);
}

//Feature detection
if (document.body.dataset) {
    each('[data-selector]', function(source) {
        document.querySelector(source.dataset.selector).innerHTML = source.innerHTML;
        source.remove();
    })
}

// Google Analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-58031952-4', 'auto');
ga('require', 'linkid', 'linkid.js');
ga('send', 'pageview');

function report(async) {
    var request = new XMLHttpRequest();
    var params = {spend: Date.now() - start};
    if (window.user) {
        params['user'] = user;
    }
    var url = '/visit.php?' + $.param(params);
    request.open('POST', url, async);
    if (window.performance) {
        trace.state.memory = performance.memory.usedJSHeapSize;
        trace.timing = performance.timing;
    }
    request.send(store('trace'));
}

var trace = {
    _type: 'trace',
    _id: start,
    _version: 2,
    state: {
        width: innerWidth,
        height: innerHeight,
        referer: document.referer
    },
    mousedown: {},
    mouseup: {},
    keyup: {}
};

if (window.addEventListener) {
    addEventListener('mousedown', function(e) {
        trace.mousedown[e.timeStamp.toString(32)] = [e.clientX, e.clientY];
    });

    addEventListener('mouseup', function(e) {
        trace.mouseup[e.timeStamp.toString(32)] = [e.clientX, e.clientY];
    });

    addEventListener('keyup', function(e) {
        var flag = 0;
        if (e.ctrlKey)  { flag |= 1; }
        if (e.altKey)   { flag |= 2; }
        if (e.shiftKey) { flag |= 4; }
        if (e.metaKey)  { flag |= 8; }
        if (e.keyCode) {
            trace.keyup[e.timeStamp.toString(32)] = [e.keyCode, flag]
        }
    });

    addEventListener('beforeunload', report);
}

function store(name) {
    if (!window['_' + name]) {
        window['_' + name] = JSON.stringify(window[name]);
    }
    return window['_' + name];
}
