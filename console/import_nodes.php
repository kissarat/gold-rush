<?php
use yii\console\Application;

require_once __DIR__ . '/../boot.php';

$app = new Application($config);

$command = $app->db->createCommand('SELECT name FROM user');
$command->execute();
$users = $command->queryColumn();
usort($users, function($a, $b) {
    return mb_strlen($b)-mb_strlen($a);
});

$command = $app->db->createCommand('SELECT * FROM mytable');
$command->execute();

$time = time();

$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();
while($row = $command->pdoStatement->fetch(PDO::FETCH_NUM)) {
    foreach($row as $i => $cell) {
        $cell = trim($cell);
        if (empty($cell)) {
            continue;
        }

        $username = '!' . $cell;
        foreach($users as $user) {
            if (mb_strpos($cell, $user) !== false) {
                $username = $user;
            }
        }
        $app->db->createCommand('INSERT INTO node(user_name, type_id, reinvest_from, `time`)
                      VALUES (:user_name, :type_id, :reinvest_from, :time)', [
            ':user_name' => $username,
            ':type_id' => $i + 1,
            ':reinvest_from' => false === mb_strpos($cell, 'реинв') ? null : 2,
            ':time' => $time++
        ])->execute();
    }
}
