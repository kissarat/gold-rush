<?php
ini_set('error_reporting', 0);
require(__DIR__ . '/../config.php');
require(__DIR__ . '/../local.php');

echo json_encode($config, JSON_UNESCAPED_UNICODE);
