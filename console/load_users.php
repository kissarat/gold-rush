<?php
use yii\console\Application;

require_once __DIR__ . '/../boot.php';

$file = $argv[1];
$file = file_get_contents($file);
$lines = explode("\n", $file);

$app = new Application($config);

foreach($lines as $line) {
    $row = explode("\t", $line);
    if (count($row) >= 1) {
        $user = trim($row[0]);
        $perfect = count($row) > 1 ? trim($row[1]) : null;
        $password = $app->security->generateRandomString(24);
        $command = $app->db->createCommand('INSERT IGNORE INTO user(`name`, `email`, `hash`, `perfect`)
            VALUES (:name, :email, :hash, :perfect)', [
            ':name' => $user,
            ':email' => $user . '@gmail.com',
            ':hash' => password_hash($password, PASSWORD_DEFAULT),
            ':perfect' => $perfect,
        ]);
        $command->execute();
        echo "$user\t$password\n";
    }
}
