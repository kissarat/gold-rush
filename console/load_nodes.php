<?php
use yii\console\Application;

require_once __DIR__ . '/../boot.php';

$file = $argv[1];
$file = file_get_contents($file);
$lines = explode("\n", $file);

$app = new Application($config);
$command = $app->db->createCommand('SELECT name FROM user');
$command->execute();
$users = $command->queryColumn();
usort($users, function($a, $b) {
    return mb_strlen($b)-mb_strlen($a);
});
//die(json_encode($users, JSON_UNESCAPED_UNICODE));

$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();
$time = time();
foreach($lines as $line) {
    $line = trim($line);
    $row = explode(";", $line);
    try {
        foreach($row as $i => $cell) {
            $cell = trim($cell);
            if (empty($cell)) {
                continue;
            }
//            foreach($users as $user) {
//                if (mb_strpos($cell, $user) !== false) {
                    $reinvest = mb_strpos($cell, 'реинв');
                    $app->db->createCommand('INSERT INTO node(user_name, type_id, reinvest_from, `time`)
                      VALUES (:user_name, :type_id, :reinvest_from, :time)', [
                        ':user_name' => $cell,
                        ':type_id' => $i + 1,
                        ':reinvest_from' => false === $reinvest ? null : 2,
                        ':time' => $time
                    ])->execute();
//                    break;
//                }
//            }
        }
    }
    catch(Exception $ex) {
        echo $ex->getMessage();
    }
}
