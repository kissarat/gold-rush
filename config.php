<?php

$config = [
    'id' => 'gold-rush-russia',
    'name' => 'Gold Rush Russian',
    'basePath' => __DIR__,
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/index',
    'language' => empty($_COOKIE['lang']) ? 'ru' : 'en',
    'charset' => 'utf-8',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\ApcCache',
            'keyPrefix' => 'grr'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'enabled' => false
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'language/<code:\w{2}>' => 'home/language',
                'signup/<authclient:\w+>' => 'user/social',
                'contact' => 'feedback/create',
                'profile/<name:[\w_\-\.]+>/edit' => 'user/update',
                'profile/<name:[\w_\-\.]+>' => 'user/view',
                'profile' => 'user/view',
                'users' => 'user/index',
                'journal/user/<user:[\w_\-\.]+>' => 'journal/index',
                'journal/<id:\d+>' => 'journal/view',
                'password/<name:[\w_\-\.]+>' => 'user/password',
                'reset/<code:[\w_\-]+>' => 'user/password',
                'feedback/template/<template:\w+>' => 'feedback/create',
                'plans' => 'matrix/plan',
                'plan/<id:\d+>' => 'matrix/view',
                'open/<id:\d+>' => 'matrix/open',
                'investment/user/<user:[\w_\-\.]+>' => 'matrix/invest',
                'investment/<id:\d+>' => 'matrix/invest',
                'investment' => 'matrix/invest',
                'income/user/<user:[\w_\-\.]+>' => 'matrix/income',
                'queue' => 'matrix/index',
                'income' => 'matrix/income',
                '<scenario:(withdraw|payment)>/user/<user:[\w_\-\.]+>' => 'invoice/index',
                '<scenario:(withdraw|payment)>/create' => 'invoice/create',
                '<scenario:(withdraw|payment)>' => 'invoice/index',
                'invoices/user/<user:[\w_\-\.]+>' => 'invoice/index',
                'invoice/<id:\d+>' => 'invoice/view',
                'referral/<referral:[\w_\-\.]+>' => 'user/signup',
                'invoices' => 'invoice/index',
                'welcome' => 'home/index',
                'signup' => 'user/signup',
                'login' => 'user/login',
                'logout' => 'user/logout',
                'answers' => 'faq/index',
                'page/<name:\w+>' => 'home/page'
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                   # 'enableCaching' => true
                ]
            ]
        ],
        'session' => [
            'class' => 'yii\web\CacheSession'
        ],
    ],
    'params' => null,
];
