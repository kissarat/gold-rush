#!/bin/bash

mkdir runtime
chmod a+w runtime
mkdir web/assets
chmod a+w web/assets
chmod a+w models
chmod a+w models/search
chmod a+w controllers
chmod a+w views
