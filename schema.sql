SET NAMES utf8;
select @@SESSION.sql_mode = "STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION";


CREATE TABLE source_message (
  id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  category VARCHAR(32) DEFAULT 'app',
  message TINYTEXT
) ENGINE MyISAM CHARACTER SET utf8 CHARACTER SET utf8;


CREATE TABLE message (
  id SMALLINT UNSIGNED,
  language VARCHAR(16) DEFAULT 'ru',
  translation TINYTEXT,
  PRIMARY KEY (id, language),
  CONSTRAINT fk_message_source_message FOREIGN KEY (id)
  REFERENCES source_message (id) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE MyISAM CHARACTER SET utf8;


CREATE VIEW translation AS
  SELECT s.id, message, translation
  FROM source_message s JOIN message t ON s.id = t.id;


DELIMITER $$
CREATE PROCEDURE translate(message TINYTEXT, translation TINYTEXT) BEGIN
  INSERT INTO source_message(`message`) VALUES (message);
  INSERT INTO message(id, `translation`) VALUES (LAST_INSERT_ID(), translation);
  END
$$
DELIMITER ;


CREATE TABLE `user` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(24) NOT NULL,
  account DECIMAL(8,2) NOT NULL DEFAULT '0.00',
  email VARCHAR(48) NOT NULL UNIQUE,
  hash CHAR(60),
  auth CHAR(64) UNIQUE,
  code CHAR(64),
  duration INT UNSIGNED NOT NULL DEFAULT 60,
  status TINYINT NOT NULL,
  perfect VARCHAR(9),
  phone VARCHAR(16),
  skype VARCHAR(32),
  facebook BIGINT UNSIGNED,
  vkontakte BIGINT UNSIGNED,
  twitter BIGINT UNSIGNED,
  last_access TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  referral VARCHAR(24),
  UNIQUE INDEX (name)
) ENGINE InnoDB CHARACTER SET utf8;
CREATE UNIQUE INDEX user_id ON `user`(`id`);
CREATE UNIQUE INDEX user_name ON `user`(`name`);

INSERT INTO `user`(name, email, status) VALUES ('admin', 'lab_tas@ukr.net', 1);


CREATE TABLE `journal` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  type VARCHAR(16) NOT NULL,
  event VARCHAR(16) NOT NULL,
  object_id INT UNSIGNED,
  user_name VARCHAR(24),
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  ip INT UNSIGNED,
  data BLOB,
  CONSTRAINT journal_user FOREIGN KEY (user_name)
    REFERENCES `user`(name)
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE InnoDB CHARACTER SET utf8;


CREATE TABLE `feedback` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(24) NOT NULL,
  email VARCHAR(48),
  subject TINYTEXT NOT NULL,
  content TEXT NOT NULL
) ENGINE InnoDB CHARACTER SET utf8;


CREATE TABLE `invoice` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_name VARCHAR(24) NOT NULL,
  amount DECIMAL(8, 2) NOT NULL,
  batch BIGINT,
  status VARCHAR(16) DEFAULT 'create',
  CONSTRAINT invoice_user FOREIGN KEY (user_name)
    REFERENCES `user`(name)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT amount CHECK (amount <> 0)
) ENGINE InnoDB CHARACTER SET utf8;


CREATE TABLE `type` (
  id SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name TINYTEXT,
  stake DECIMAL(8,2) NOT NULL,
  income DECIMAL(8,2) NOT NULL,
  degree TINYINT UNSIGNED NOT NULL,
  invest_id SMALLINT UNSIGNED,
  reinvest TINYINT UNSIGNED,
  next_id SMALLINT UNSIGNED,
  CONSTRAINT type_next FOREIGN KEY (next_id)
    REFERENCES `type`(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT stake CHECK (stake > 0)
) ENGINE InnoDB CHARACTER SET utf8;

INSERT INTO type (id, name,    stake,  income,  degree, invest_id, reinvest, next_id) VALUES
                 (5, 'План-5', 225.00, 1000.00, 5,      1,         0,       null   ),
                 (4, 'План-4', 90.00,  100.00,  5,      1,         6,        5      ),
                 (3, 'План-3', 45.00,  30.00,   4,      1,         3,        4      ),
                 (2, 'План-2', 30.00,  20.00,   3,      1,         1,        3      ),
                 (1, 'План-1', 20.00,  5.00,    2,      null,      null,     2      );


CREATE TABLE `node` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_name VARCHAR(24) NOT NULL,
  type_id SMALLINT UNSIGNED NOT NULL,
  count TINYINT UNSIGNED NOT NULL,
  reinvest_from SMALLINT UNSIGNED,
  time INT UNSIGNED NOT NULL,
  CONSTRAINT node_user FOREIGN KEY (user_name)
    REFERENCES `user`(name)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT node_type FOREIGN KEY (type_id)
    REFERENCES `type`(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `count` CHECK (count >= 0)
) ENGINE InnoDB CHARACTER SET utf8;


CREATE VIEW `matrix` AS
  SELECT n.id as id, type_id, user_name, n.reinvest_from, `time`
  FROM node n JOIN type t ON n.type_id = t.id;


CREATE TABLE `archive` (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  node_id INT UNSIGNED NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  type_id SMALLINT UNSIGNED NOT NULL,
  reinvest_from SMALLINT UNSIGNED,
  time INT UNSIGNED NOT NULL
) ENGINE InnoDB CHARACTER SET utf8;


CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` TINYTEXT NOT NULL,
  `answer` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE MyISAM CHARACTER SET utf8;


CREATE TABLE `visit_agent` (
  `id` SERIAL PRIMARY KEY,
  `agent` VARCHAR(200),
  `ip` INT UNSIGNED
) ENGINE MyISAM CHARACTER SET utf8;
CREATE UNIQUE INDEX visit_agent_ip ON `visit_agent`(`ip`);


CREATE TABLE `visit_path` (
  `id` SERIAL PRIMARY KEY,
  `agent_id` INT NOT NULL,
  `path` VARCHAR(80) NOT NULL,
  `spend` SMALLINT,
  `time` TIMESTAMP DEFAULT current_timestamp,
  `data` TEXT,
  CONSTRAINT user_agent FOREIGN KEY (agent_id)
  REFERENCES `visit_agent`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE MyISAM CHARACTER SET utf8;


CREATE VIEW `visit` AS
  SELECT p.id as id, agent_id, spend, user_name, `path`, p.`time`, INET_NTOA(a.ip), agent FROM visit_path p
    JOIN visit_agent a ON agent_id = a.id
    LEFT JOIN journal j ON a.ip = j.ip and user_name is not null
  GROUP BY p.id, user_name, a.ip, agent;
