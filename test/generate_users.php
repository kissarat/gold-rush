<?php
use yii\console\Application;

require_once __DIR__ . '/../boot.php';

$app = new Application($config);

//SET FOREIGN_KEY_CHECKS=0;
//$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();

for($i = 2; $i < $argv[1]; $i++) {
    $name = 'user' . $i;
    $app->db->createCommand('INSERT INTO `user`(name, email, status) VALUES (:name, :email, 2)', [
        ':name' => $name,
        ':email' => $name . '@yopmail.com'
    ])->execute();
}

$app->db->createCommand('UPDATE user SET account = 1000, hash = "$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W"')->execute();
