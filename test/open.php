<?php
use app\models\User;
use tests\Form;
use yii\console\Application;

require_once __DIR__ . '/../boot.php';

$app = new Application($config);

require_once 'Form.php';

function open_matrix($username)
{
    $form = new Form('/user/login', 'Login');
    $form->fill([
        'name' => $username,
        'password' => '1',
    ]);
    $form->send();

    $raw = $form->go('/matrix/open?id='. rand(1, 5));//ceil(pow(rand(0, 5) / mt_getrandmax(), 1)));
    file_put_contents(__DIR__ . "/../web/out/$username.html", $raw);
    $form->go('/user/logout');
}

$users = User::find()->select('name')->where('id >= ' . (isset($argv[1]) ? $argv[1] : 1))->orderBy(['id' => SORT_ASC])->all();

foreach($users as $user) {
    echo "# User: $user->name\n";
    open_matrix($user->name);
}
