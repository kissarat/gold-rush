<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/18/15
 * Time: 9:06 AM
 */

namespace app\models;


use yii\db\ActiveRecord;

class Faq extends ActiveRecord {

    public static function tableName() {
        return 'faq';
    }

    public function rules() {
        return [
            [['question', 'answer'], 'required']
        ];
    }

    public function attributeLabels() {
        return [
            'question' => 'Вопрос',
            'answer' => 'Ответ'
        ];
    }
}