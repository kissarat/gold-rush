<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "archive".
 *
 * @property integer $id
 * @property integer $node_id
 * @property string $user_name
 * @property integer $type_id
 * @property integer $reinvest_from
 * @property integer $time
 */
class Income extends ActiveRecord
{
    public static function tableName() {
        return 'archive';
    }

    public function rules() {
        return [
            [['node_id', 'user_name', 'type_id', 'time'], 'required'],
            [['node_id', 'type_id', 'reinvest_from', 'time'], 'integer'],
            [['user_name'], 'string', 'max' => 24]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'node_id' => Yii::t('app', 'Investment ID'),
            'user_name' => Yii::t('app', 'Username'),
            'type_id' => Yii::t('app', 'Plan'),
            'reinvest_from' => Yii::t('app', 'Reinvest From'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return Type
     */
    public function getType() {
        return Type::get($this->type_id);
    }

    /**
     * @return Type
     */
    public function getReinvest() {
        return Type::get($this->reinvest_from);
    }

    /**
     * @return ActiveQuery
     */
    public function getNode() {
        return $this->hasOne(Node::className(), ['id' => 'node_id']);
    }
}
