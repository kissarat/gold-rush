<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matrix".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $user_name
 * @property integer $count
 * @property integer $reinvest_from
 * @property integer $time
 */
class Matrix extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matrix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type_id', 'count', 'reinvest_from', 'time'], 'integer'],
            [['type_id', 'user_name', 'time'], 'required'],
            [['user_name'], 'string', 'max' => 24]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'user_name' => 'User Name',
            'count' => 'Count',
            'reinvest_from' => 'Reinvest From',
            'time' => 'Time',
        ];
    }
}
