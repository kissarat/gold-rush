<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "translation".
 *
 * @property integer $id
 * @property string $message
 * @property string $translation
 */
class Translation extends ActiveRecord
{
    public static function tableName() {
        return 'translation';
    }

    public static function primaryKey() {
        return ['id'];
    }

    public function rules() {
        return [
            [['id'], 'integer'],
            [['message', 'translation'], 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'message' => Yii::t('app', 'Message'),
            'translation' => Yii::t('app', 'Translation'),
        ];
    }
}
