<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * @property string $title
 * @property string $content
 */
class Article extends Model {
    public $name;
    public $content;

    public function rules()
    {
        return [
            [['content', 'name'], 'required'],
            [['content'], 'string', 'min' => 20],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'content' => 'Текст',
        ];
    }

    public function getTitle() {
        return preg_match('|<h1>([^<]+)</h1>|', $this->content, $match) ? $match[1] : '';
    }

    public static function getFileName($name) {
        return Yii::$app->getBasePath() . "/web/page/$name.html";
    }

    public static function fromFile($name) {
        $article = new Article([
            'name' => $name,
            'content' => @file_get_contents(static::getFileName($name))
        ]);
        return $article;
    }

    public function loadFile() {
        $this->content = @file_get_contents(static::getFileName($this->name));
    }

    public function saveFile() {
        return file_put_contents(static::getFileName($this->name), $this->content);
    }
}
