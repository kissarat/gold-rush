<?php

namespace app\models;


use app\behaviors\Journal;
use Exception;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\User as WebUser;

/**
 * Class User
 * @property integer id
 * @property string name
 * @property string auth
 * @property string email
 * @property string code
 * @property string hash
 * @property string wallet
 * @property string referral
 * @property integer status
 * @property number account
 * @property array socials
 */
class User extends ActiveRecord implements IdentityInterface {

    const BLOCKED = 0;
    const ADMIN = 1;
    const PLAIN = 2;
    const MANAGER = 3;

    public $social;
    private $_password;

    public static function primaryKey() {
        return ['id'];
    }

    public static function statuses() {
        return [
            User::BLOCKED => Yii::t('app', 'Blocked'),
            User::ADMIN => Yii::t('app', 'Admin'),
            User::PLAIN => Yii::t('app', 'Registered'),
            User::MANAGER => Yii::t('app', 'Manager')
        ];
    }

    public static $events = [
        'login' => 'Вход',
        'logout' => 'Выход',
        'login_fail' => 'Неудачная попытка входа',
    ];

    public function traceable() {
        return ['status', 'email', 'skype', 'phone', 'perfect', 'account', 'facebook', 'vkontakte', 'twitter'];
    }

    public function url() {
        return ['user/view', 'name' => $this->name];
    }

    public function rules() {
        return [
            ['id', 'integer'],
            [['name', 'email', 'perfect', 'account'], 'required'],
            ['password', 'required', 'on' => 'signup'],
            [['name', 'referral'], 'string', 'min' => 4, 'max' => 24],
            [['name', 'referral'], 'match', 'pattern' => '/^[a-z][a-z0-9_\-]+$/i'],
            ['email', 'email'],
            ['perfect', 'match', 'pattern' => '/^U\d{7,8}$/', 'message' =>
                Yii::t('app', 'The wallet should looks like U1234567')],
            ['skype', 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z0-9\.,\-_]{5,31}$/'],
            ['phone', 'match', 'pattern' => '/^\d{9,16}$/', 'message' =>
                Yii::t('app', 'The phone number should contain only digits and include country and area code')],

            [['name', 'email', 'skype', 'phone', 'perfect', 'referral'], 'filter', 'filter' => 'trim'],
            ['name', 'unique',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'This value has already been taken')],
            ['referral', 'exist',
                'targetClass' => 'app\models\User',
                'targetAttribute' => 'name',
                'message' => Yii::t('app', 'Referral does not exists')],
        ];
    }

    public function scenarios() {
        return [
            'default' => ['name', 'email', 'phone', 'skype'],
            'signup'  => ['name', 'email', 'phone', 'skype', 'perfect', 'password', 'referral'],
            'admin'   => ['name', 'email', 'phone', 'skype', 'perfect', 'account', 'status', 'referral'],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'account' => Yii::t('app', 'Account'),
            'skype' => Yii::t('app', 'Skype'),
            'phone' => Yii::t('app', 'Phone'),
            'perfect' => Yii::t('app', 'Perfect Money wallet'),
        ];
    }

    public function behaviors() {
        return [
            Journal::className()
        ];
    }

    public function init() {
        parent::init();
        if (isset(Yii::$app->user)
            && Yii::$app->user instanceof WebUser
            && !Yii::$app->user->getIsGuest()
            && static::ADMIN == Yii::$app->user->identity->status) {
            $this->scenario = 'admin';
        }
    }

    /**
     * @param string $id
     * @return User
     */
    public static function findIdentity($id) {
        return parent::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new Exception('Not implemented findIdentityByAccessToken');
    }

    /**
     * @param $name
     * @param $id
     * @return User
     */
    public static function findSocial($name, $id) {
        return static::findOne([$name => $id]);
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->auth;
    }

    public function validateAuthKey($authKey) {
        return $authKey == $this->auth;
    }

    public function validatePassword($password) {
        return password_verify($password, $this->hash);
    }

    public function getPassword() {
        return $this->_password;
    }

    public function setPassword($value) {
        $this->hash = password_hash($value, PASSWORD_DEFAULT);
        $this->_password = $value;
    }

    public function generateAuthKey() {
        $this->auth = Yii::$app->security->generateRandomString(64);
    }

    public function generateCode() {
        $this->code = Yii::$app->security->generateRandomString(64);
    }

    public function isManager() {
        return static::ADMIN == $this->status || static::MANAGER == $this->status;
    }

    public function isAdmin() {
        return static::ADMIN == $this->status;
    }

    public function journal(Record $record) {
    }

    public function __toString() {
        return $this->name;
    }

    public function sendEmail($template, $params)
    {
        $template = Yii::getAlias('@app') . "/mail/$template.php";
        return Yii::$app->mailer->compose()
            ->setTo($this->email)
            ->setFrom(['gold-rush-russia@mail.ru' => Yii::$app->name])
            ->setSubject($params['subject'])
            ->setHtmlBody(Yii::$app->view->renderFile($template, $params))
            ->send();
    }

    public function canLogin() {
        return Record::find()->andWhere([
            'object_id' => $this->id,
            'event' => 'login_fail'
        ])
            ->andWhere('time > DATE_SUB(NOW(), INTERVAL 5 MINUTE)')
            ->count() < 30;
    }

    public function getReferralUser() {
        return $this->hasOne(User::class, ['name' => 'referral']);
    }
}
