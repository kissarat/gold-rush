<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "type".
 *
 * @property integer $id
 * @property string $name
 * @property number $stake
 * @property number $income
 * @property integer $degree
 * @property integer $next_id
 * @property integer $invest_id
 * @property integer $invest_number
 *
 * @property Type $next
 */
class Type extends ActiveRecord
{
    private static $_all;

    public static function tableName() {
        return 'type';
    }

    public function rules(){
        return [
            [['name'], 'string'],
            [['stake', 'income'], 'required'],
            [['stake', 'income'], 'number'],
            [['next_id'], 'integer']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'stake' => Yii::t('app', 'Enter'),
            'degree' => Yii::t('app', 'Degree'),
            'income' => Yii::t('app', 'Exit'),
            'next_id' => Yii::t('app', 'Exit'),
            'invest_id' => Yii::t('app', 'Reinvest'),
            'reinvest' => Yii::t('app', 'Reinvest Count'),
        ];
    }

    /**
     * @return Type
     */
    public function getNext() {
        return static::get($this->next_id);
    }

    /**
     * @return Type
     */
    public function getInvest() {
        return static::get($this->invest_id);
    }

    /**
     * @return Type[]
     */
    public static function all() {
        if (!static::$_all) {
            $types = static::find()->all();
            static::$_all = [];
            foreach($types as $type) {
                static::$_all[$type->id] = $type;
            }
        }
        return static::$_all;
    }

    public static function getNames() {
        $names = [];
        foreach(static::all() as $type) {
            $names[$type->id] = $type->name;
        }
        return $names;
    }

    /**
     * @param $id
     * @return Type|null
     */
    public static function get($id) {
        $types = Type::all();
        return isset($types[$id]) ? $types[$id] : null;
    }

    public function __toString() {
        return $this->name;
    }
}