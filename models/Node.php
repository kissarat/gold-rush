<?php

namespace app\models;

use Exception;
use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "node".
 *
 * @property integer $id
 * @property string $user_name
 * @property integer $type_id
 * @property integer $count
 * @property integer $time
 * @property integer $reinvest_from
 *
 * @property Type $type
 * @property User $user
 * @property Node[] $children
 */
class Node extends ActiveRecord
{
    private $_children;

    public static function tableName() {
        return 'node';
    }

    public function rules() {
        return [
            [['user_name', 'type_id', 'time'], 'required'],
            [['type_id', 'count', 'time'], 'integer'],
            [['user_name'], 'string', 'max' => 24],
            ['time', 'default', 'value' => $_SERVER['REQUEST_TIME']]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_name' => Yii::t('app', 'Username'),
            'type_id' => Yii::t('app', 'Plan'),
            'reinvest_from' => Yii::t('app', 'Reinvest From'),
            'count' => Yii::t('app', 'Counter'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['name' => 'user_name']);
    }

    public function setUser(User $value) {
        $this->user_name = $value->name;
    }

    /**
     * @return Type
     */
    public function getType() {
//        return Type::get($this->type_id);
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    public function setType(User $value) {
        $this->type_id = $value->id;
    }

    public function getChildren() {
        if (!$this->_children) {
            $this->_children = Node::find()->where(['>', 'id', $this->id])->limit(40)->all();
        }
        return $this->_children;
    }

    public function countQueue() {
        return static::find()
            ->andWhere('time <= :time', [':time' => $this->time])
            ->andWhere(['type_id' => $this->type_id])
            ->sum('count');
    }

    public function decrement() {
        $this->count--;
        return $this->update(false, ['count']);
    }

    public static function rise() {
        $i = 0;
        $types = Type::all();
        foreach($types as $id => $type) {
            $previous = static::find()->where(['type_id' => $id])
                ->andWhere('count <= 0')
                ->all();
            foreach ($previous as $node) {
                $node->up();
                $i++;
            }
        }
        return $i;
    }

    public function up() {
        Yii::$app->db->createCommand(
            'INSERT INTO archive(node_id,  user_name,  type_id,  reinvest_from,  time)
                        VALUES (:node_id, :user_name, :type_id, :reinvest_from, :time)', [
            ':node_id' => $this->id,
            ':user_name' => $this->user_name,
            ':type_id' => $this->type_id,
            ':reinvest_from' => $this->reinvest_from,
            ':time' => $this->time
        ])
            ->execute();
        $user = $this->user;
        $this->reinvest();
        $user->account += (float) $this->type->income;
        if ($this->type->next_id) {
            $this->type_id = $this->type->next_id;
            if (!$this->invest()) {
                throw new Exception($this->dump());
            }
        }
        else {
            $this->delete();
        }
        if (!$user->update(false, ['account'])) {
            throw new Exception($this->dump());
        }
    }

    public function invest() {
        $this->count = $this->type->degree + ($this->type_id > 1 ? 1 : 0);
        $this->time = $_SERVER['REQUEST_TIME'];
        $expectant = static::find()->where(['type_id' => $this->type_id])->andWhere('count > 0')
            ->orderBy(['time' => SORT_ASC,'id' => SORT_ASC])->limit(1)->one();
        if ($expectant) {
            $expectant->decrement();
        }
        return $this->save();
    }

    public function reinvest() {
        if ($this->type->invest_id && $this->type->reinvest > 0) {
            for($i = 0; $i < $this->type->reinvest; $i++) {
                $node = new static([
                    'type_id' => $this->type->invest_id,
                    'user_name' => $this->user_name,
                    'reinvest_from' => $this->type_id
                ]);
                $node->invest();
            }
        }
    }

    public function dump() {
        $bundle = [
            'id' => $this->id,
            'type_id' => $this->type_id,
            'count' => $this->count,
            'time' => date($this->time),
            'user_name' => $this->user_name,
            'account' => $this->user->account
        ];
        if (count($this->errors) > 0) {
            $bundle['errors'] = $this->errors;
        }
        if (count($this->user->errors) > 0) {
            $bundle['user'] = $this->user->attributes;
            $bundle['user_errors'] = $this->user->errors;
        }
        return json_encode($bundle, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function __toString() {
        return $this->id . ' ' . Type::get($this->type_id);
    }
}
