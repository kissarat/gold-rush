<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 7/6/15
 * Time: 1:20 AM
 */

namespace app;


use Yii;

class SQL {
    public static function query($sql, $params = null) {
        $command = Yii::$app->db->createCommand($sql, $params);
        $command->execute();
        return $command;
    }

    public static function queryObject($sql, $params = null) {
        return static::query($sql, $params)->pdoStatement->fetchObject();
    }

    public static function queryColumn($sql, $params = null) {
        return static::query($sql, $params)->queryColumn();
    }

    public static function queryCell($sql, $params = null) {
        return static::query($sql, $params)->pdoStatement->fetchColumn();
    }
}
